<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sas
 */
get_header(); ?>

    <div class="container">
        <div class="row">
            <main class="col-lg-6 col-lg-offset-1 col-lg-push-2 col-md-9 col-sm-8">

				<?php
				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/content', 'realty-single' );
				endwhile;
				the_posts_pagination();
				?>

            </main>
            <aside class="col-lg-2 col-lg-pull-7 col-md-3 col-sm-4">

				<?php dynamic_sidebar( 'sidebar-left' ); ?>

            </aside>
            <aside class="col-lg-2 col-lg-offset-1 col-md-12 col-sm-12">

				<?php dynamic_sidebar( 'sidebar-right' ); ?>

            </aside>
        </div>
    </div>

<?php
get_footer();
