$(function () {

    $('.main-screen-slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: true
    });

    $('.map-ajax-popup').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });

    // Галерея на странице архива (ленивая подгрузка)
    $('.realty-archive-gallery').slick({
        lazyLoad: 'ondemand',
        slidesToShow: 1
    });

    //  Галерея на одиночной странице
    $('.realty-single-slider-for').slick({
        lazyLoad: 'ondemand',
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.realty-single-slider-nav'
    });
    $('.realty-single-slider-nav').slick({
        lazyLoad: 'ondemand',
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.realty-single-slider-for',
        dots: false,
        focusOnSelect: true,
        centerMode: true
    });

});
