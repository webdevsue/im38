var 	gulp           = require('gulp'),
    gutil          = require('gulp-util' ),
    sass           = require('gulp-sass'),
    browserSync    = require('browser-sync'),
    concat         = require('gulp-concat'),
    uglify         = require('gulp-uglify'),
    cleanCSS       = require('gulp-clean-css'),
    rename         = require('gulp-rename'),
    imagemin       = require('gulp-imagemin'),
    pngquant       = require('imagemin-pngquant'),
    cache          = require('gulp-cache'),
    autoprefixer   = require('gulp-autoprefixer'),
    fileinclude    = require('gulp-file-include'),
    bourbon        = require('node-bourbon'),
    notify         = require("gulp-notify");

gulp.task('browser-sync', function() {

    var files = [
        '../../**/*.php'
    ];

    browserSync.init(files, {
        proxy: "newim38.loc",	//	Заменить на УРЛ сайта.
        notify: false
    });
});

gulp.task('sass', function() {
    return gulp.src('../sass/**/*.{scss,sass}')
        .pipe(sass({
            includePaths: bourbon.includePaths
        }).on("error", notify.onError()))
        .pipe(rename({suffix: '.min', prefix : ''}))
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(cleanCSS())
        .pipe(gulp.dest('../../assets/css'))
        .pipe(browserSync.reload({stream: true}))
});


gulp.task('libs', function() {
    return gulp.src([
        '../libs/jquery/dist/jquery.min.js',
        '../libs/slick/slick/slick.min.js',
        '../libs/Magnific-Popup/dist/jquery.magnific-popup.js',
        '../libs/selectize.js/dist/js/standalone/selectize.min.js',
        //'../libs/blazy/blazy.min.js',
        // '../libs/owl/owl.carousel.min.js',
        // '../libs/jquery-match-height/dist/jquery.matchHeight-min.js',
        // '../libs/Magnific-Popup/dist/jquery.magnific-popup.js',
        // '../libs/accordion/js/QuickAccord.min.js',
        '../common.js' // Всегда в конце
    ])
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../../assets/js'));
});

gulp.task('watch', ['sass', 'libs', 'browser-sync'], function() {
    gulp.watch('../sass/{,*/}*.{scss,sass}', ['sass']);
    gulp.watch('../common.js', ['libs']);
    gulp.watch('../common.js', browserSync.reload);
});

gulp.task('imagemin', function() {
    return gulp.src('../assets/img/**/*')
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('dist/img'));
});

gulp.task('default', ['watch']);

gulp.task('clearcache', function () { return cache.clearAll(); });