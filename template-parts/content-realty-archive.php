<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sas
 */

?>

<? //= do_shortcode( '[main_search_form]' ) ?>

<?php
$gallery  = get_field( 'sas_realty_gallery' );
$area     = get_post_meta( $post->ID, 'sas_realty_area', true );
$price    = get_post_meta( $post->ID, 'sas_realty_price', true );
$date     = get_the_date( 'd.m.Y' );
$floorOf  = get_post_meta( $post->ID, 'sas_realty_floor-of', true );
$floor    = get_post_meta( $post->ID, 'sas_realty_floor', true );
$rooms    = get_post_meta( $post->ID, 'sas_realty_number-of-rooms', true );
$address  = get_post_meta( $post->ID, 'sas_realty_address', true );
$district = get_field('sas_realty_district');
$terms    = get_terms( array(
	'taxonomy'   => 'zhilyye-kompleksy',
	'hide_empty' => true,
) );
$zhk      = $terms[0]->name;
?>
<div class="realty-archive-item">
    <div class="row">
        <div class="col-md-6">
			<?php if ( $gallery ): ?>
                <div class="realty-archive-gallery">
					<?php
					foreach ( $gallery as $image ): ?>
                        <div>
                            <img data-lazy="<?= kama_thumb_src( 'w=280 &h=180', $image['url'] ) ?>"/>
                        </div>
					<?php endforeach; ?>
                </div>
				<?php
			endif;
			?>
            <i class="gallery-icon"></i>
        </div>
        <div class="col-md-6">
            <div class="row">
                <a href="<?php the_permalink() ?>">
                    <h2>
						<?php
						if ( get_post_meta( $post->ID, 'sas_realty_type', true ) == 'apartment' ):;
							echo $rooms . '-к квартира, ' . $area . 'м<sup>2</sup>, ' . $floor . '/' . $floorOf . ' этаж';
                        elseif ( get_post_meta( $post->ID, 'sas_realty_type', true ) == 'house' ):
							echo 'Дом на ул. ' . $address;
						endif;
						?>
                    </h2>
                </a>
                <div class="description">
					<?php if ( $district ) : echo "р-н " . $district['label'] . ", "; endif; ?>
					<?php if ( $address ) : echo "ул. $address, "; endif; ?>
					<?php if ( $zhk ) : echo "$zhk"; endif; ?>
                </div>
                <div class="date"><small><?=$date?></small></div>
                <div class="row">
                    <div class="yellow-panel">
                        <div class="price">
                            <strong><?= number_format( $price, '0', '.', ' ' ) ?> руб.</strong>
                        </div>
                        <a href="#" class="reset cta">запись на просмотр <i class="eye"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ( get_edit_post_link() ) : ?>
    <footer class="entry-footer">
		<?php
		edit_post_link(
			sprintf(
			/* translators: %s: Name of current post */
				esc_html__( 'Редактировать %s', 'sas' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			),
			'<span class="edit-link">',
			'</span>'
		);
		?>
    </footer><!-- .entry-footer -->
<?php endif; ?>
