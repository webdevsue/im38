<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sas
 */
?>

<?php
$gallery  = get_field( 'sas_realty_gallery' );
$area     = get_post_meta( $post->ID, 'sas_realty_area', true );
$price    = get_post_meta( $post->ID, 'sas_realty_price', true );
$date     = get_the_date( 'd.m.Y' );
$floorOf  = get_post_meta( $post->ID, 'sas_realty_floor-of', true );
$floor    = get_post_meta( $post->ID, 'sas_realty_floor', true );
$rooms    = get_post_meta( $post->ID, 'sas_realty_number-of-rooms', true );
$address  = get_post_meta( $post->ID, 'sas_realty_address', true );
$district = get_field( 'sas_realty_district' );
$terms    = get_terms( array(
	'taxonomy'   => 'zhilyye-kompleksy',
	'hide_empty' => true,
) );
$zhk      = $terms[0]->name;
?>

<h1>
	<?php
	if ( get_post_meta( $post->ID, 'sas_realty_type', true ) == 'apartment' ):;
		echo $rooms . '-к квартира, ' . $area . 'м<sup>2</sup>, ' . $floor . '/' . $floorOf . ' этаж';
    elseif ( get_post_meta( $post->ID, 'sas_realty_type', true ) == 'house' ):
		echo 'Дом на ул. ' . $address;
	endif;
	?>
</h1>

<?php if ( $gallery ): ?>
    <div class="realty-single-gallery">
        <div class="realty-single-slider-for">
			<?php
			foreach ( $gallery as $image ): ?>
                <div>
                    <img data-lazy="<?= kama_thumb_src( 'w=500 &h=375', $image['url'] ) ?>"/>
                </div>
			<?php endforeach; ?>
        </div>
        <div class="realty-single-slider-nav">
		    <?php
		    foreach ( $gallery as $image ): ?>
                <div>
                    <img data-lazy="<?= kama_thumb_src( 'w=100 &h=100', $image['url'] ) ?>"/>
                </div>
		    <?php endforeach; ?>
        </div>
    </div>
	<?php
endif;
?>
