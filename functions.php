<?php
/**
 * sas functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package sas
 */

if ( ! function_exists( 'sas_setup' ) ) :
	function sas_setup() {
		load_theme_textdomain( 'sas', get_template_directory() . '/languages' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Верхнее', 'sas' ),
		) );
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
		add_theme_support( 'custom-background', apply_filters( 'sas_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif;
add_action( 'after_setup_theme', 'sas_setup' );

function sas_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sas_content_width', 640 );
}

add_action( 'after_setup_theme', 'sas_content_width', 0 );

function sas_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'sas' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'sas' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Left sidebar', 'sas' ),
		'id'            => 'sidebar-left',
		'description'   => esc_html__( 'Add widgets here.', 'sas' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Right sidebar', 'sas' ),
		'id'            => 'sidebar-right',
		'description'   => esc_html__( 'Add widgets here.', 'sas' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}

add_action( 'widgets_init', 'sas_widgets_init' );

function sas_scripts() {
	wp_enqueue_style( 'sas-style', get_stylesheet_uri() );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'sas_scripts' );

//require get_template_directory() . '/inc/custom-header.php';
//require get_template_directory() . '/inc/extras.php';
//require get_template_directory() . '/inc/customizer.php';
//require get_template_directory() . '/inc/jetpack.php';
require get_template_directory() . '/inc/template-tags.php';


/**
 *  My custom functions
 */

add_action( 'after_setup_theme', 'sas_clean_head' );
function sas_clean_head() {

	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wp_shortlink_wp_head' );
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10 );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'feed_links', 2 );

	remove_action( 'wp_print_styles', 'print_emoji_styles' );

	add_filter( 'the_generator', '__return_false' );
	add_filter( 'feed_links_show_comments_feed', '__return_false' );
	add_filter( 'emoji_svg_url', '__return_false' );
	//add_filter('show_admin_bar','__return_false');

}

/**
 * Удаляет инлайновый CSS в wp-head
 */
add_action( 'wp_enqueue_scripts', 'remove_embedded_style' );
function remove_embedded_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head',
		array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}

/**
 * Полностью отключает REST API
 */
add_filter( 'rest_enabled', '__return_false' );

/**
 * Отключаем фильтры REST API
 */
remove_action( 'xmlrpc_rsd_apis', 'rest_output_rsd' );
remove_action( 'wp_head', 'rest_output_link_wp_head', 10, 0 );
remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
remove_action( 'auth_cookie_malformed', 'rest_cookie_collect_status' );
remove_action( 'auth_cookie_expired', 'rest_cookie_collect_status' );
remove_action( 'auth_cookie_bad_username', 'rest_cookie_collect_status' );
remove_action( 'auth_cookie_bad_hash', 'rest_cookie_collect_status' );
remove_action( 'auth_cookie_valid', 'rest_cookie_collect_status' );
remove_filter( 'rest_authentication_errors', 'rest_cookie_check_errors', 100 );

/**
 * Отключаем события REST API
 */
remove_action( 'init', 'rest_api_init' );
remove_action( 'rest_api_init', 'rest_api_default_filters', 10, 1 );
remove_action( 'parse_request', 'rest_api_loaded' );

/**
 * Отключаем Embeds связанные с REST API
 */
remove_action( 'rest_api_init', 'wp_oembed_register_route' );
remove_filter( 'rest_pre_serve_request', '_oembed_rest_pre_serve_request', 10, 4 );

/**
 * если собираетесь выводить вставки из других сайтов на своем, то закомментируйте след. строку.
 */
remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
remove_action( 'wp_head', 'wp_oembed_add_host_js' );

/**
 * Отключает feed в wp_head
 */
function sas_disable_feed() {
	wp_die( 'Нет доступных каналов новостей, пожалуйста, перейдите на  <a href="' . esc_url( home_url( '/' ) ) . '">главную страницу</a>!' );
}

add_action( 'do_feed', 'sas_disable_feed', 1 );
add_action( 'do_feed_rdf', 'sas_disable_feed', 1 );
add_action( 'do_feed_rss', 'sas_disable_feed', 1 );
add_action( 'do_feed_rss2', 'sas_disable_feed', 1 );
add_action( 'do_feed_atom', 'sas_disable_feed', 1 );
add_action( 'do_feed_rss2_comments', 'sas_disable_feed', 1 );
add_action( 'do_feed_atom_comments', 'sas_disable_feed', 1 );


/**
 * Шорткод для логотипа
 */
function logo__func() {
	if ( ! is_front_page() ) { ?>
        <a href="<?= home_url( '/' ) ?>" class="reset logo" title="Перейти на главную">
            <img src="<?= get_template_directory_uri() ?>/assets/img/logo.svg" class="logo"
                 alt="ЛОГОТИП">
        </a>
	<?php } else { ?>
        <div class="logo">
            <img src="<?= get_template_directory_uri() ?>/assets/img/logo.svg" class="logo"
                 alt="ЛОГОТИП">
        </div>
	<?php }
}

add_shortcode( 'logo', 'logo__func' );

/**
 * Шорткод для основной формы
 */
function main_search_form__func( $attr ) {
	?>
    <form action="" class="search-form <?php if ( is_front_page() ):echo 'search-form--shadow'; endif; ?>">
        <div class="row">
            <div class="col-sm-6">
                <div class="rooms">
                    <label for="rooms" class="label">Количество комнат:</label>
                    <select name="rooms" id="rooms">
                        <option value="one">Одна</option>
                        <option value="two">Две</option>
                        <option value="three">Три</option>
                        <option value="four">Четыре +</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="rooms">
                    <label for="rooms" class="label">Район:</label>
                    <select name="rooms" id="rooms">
                        <option value="one">Одна</option>
                        <option value="two">Две</option>
                        <option value="three">Три</option>
                        <option value="four">Четыре +</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="rooms">
                    <label for="rooms" class="label">Стоимость ОТ (руб.):</label>
                    <select name="rooms" id="rooms">
                        <option value="one">1000000</option>
                        <option value="two">1100000</option>
                        <option value="three">1200000</option>
                        <option value="four">1300000</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="rooms">
                    <label for="rooms" class="label">Стоимость ДО (руб.):</label>
                    <select name="rooms" id="rooms">
                        <option value="one">1000000</option>
                        <option value="two">1100000</option>
                        <option value="three">1200000</option>
                        <option value="four">1300000</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-12">
                <span class="label">Форма оплаты:</span>
                <input type="checkbox" name="vehicle" value="Bike">
                <label for="venicle">Выбрать всё</label>
                <input type="checkbox" name="vehicle1" value="Bike">
                <label for="venicle1">Материнский капитал</label>
                <input type="checkbox" name="vehicle2" value="Car">
                <label for="venicle2">Военная ипотека</label>
            </div>
            <div class="col-md-6">
                <label for="s" class="label">Стоимость ДО (руб.):</label>
                <select name="s" class="select">
                    <option selected disabled>-</option>
                    <option value="one">One</option>
                    <option value="two">Two</option>
                </select>
            </div>
            <div class="col-md-6">
                <div class="control">
                    <input class="control__input" id="Radio" type="radio">
                    <label class="control__label" for="Radio">Radio</label>
                </div>
                <div class="control">
                    <input class="control__input" id="checkbox" type="checkbox">
                    <label class="control__label" for="checkbox">Checkbox</label>
                </div>
            </div>
        </div>


    </form>
	<?php
}

add_shortcode( 'main_search_form', 'main_search_form__func' );

/**
 * Регистрация post type
 */
add_action( 'init', 'sas_register_post_type' );
function sas_register_post_type() {
	register_post_type( 'sas_realty',
		array(
			'labels'      => array(
				'name'          => __( 'Каталог недвижимости' ),
				'singular_name' => __( 'Объект недвижимости' )
			),
			'public'      => true,
			'has_archive' => true,
			'rewrite'     => array( 'slug' => 'nedvizhimost' ),
			'menu_icon'   => 'dashicons-admin-multisite'
		)
	);
	register_post_type( 'sas_slider',
		array(
			'labels'      => array(
				'name'          => __( 'Слайдер' ),
				'singular_name' => __( 'Слайдер' )
			),
			'public'      => true,
			'has_archive' => false,
			'menu_icon'   => 'dashicons-slides',
			'supports'    => array( 'title', 'editor', 'thumbnail' )
		)
	);
}

/**
 * API KEY для ACF
 */
function my_acf_init() {
	acf_update_setting( 'google_api_key', 'AIzaSyA84NFdZN_hI0s0VaDUoas2JZXkI-vigj0' );
}

add_action( 'acf/init', 'my_acf_init' );


/**
 * Хлебные крошки для WordPress (breadcrumbs)
 *
 * @param  string [$sep  = '']      Разделитель. По умолчанию ' » '
 * @param  array [$l10n = array()] Для локализации. См. переменную $default_l10n.
 * @param  array [$args = array()] Опции. См. переменную $def_args
 *
 * @return string Выводит на экран HTML код
 *
 * version 3.3.1
 */
function kama_breadcrumbs( $sep = ' » ', $l10n = array(), $args = array() ) {
	$kb = new Kama_Breadcrumbs;
	echo $kb->get_crumbs( $sep, $l10n, $args );
}

class Kama_Breadcrumbs {

	public $arg;

	// Локализация
	static $l10n = array(
		'home'       => 'Главная',
		'paged'      => 'Страница %d',
		'_404'       => 'Ошибка 404',
		'search'     => 'Результаты поиска по запросу - <b>%s</b>',
		'author'     => 'Архив автора: <b>%s</b>',
		'year'       => 'Архив за <b>%d</b> год',
		'month'      => 'Архив за: <b>%s</b>',
		'day'        => '',
		'attachment' => 'Медиа: %s',
		'tag'        => 'Записи по метке: <b>%s</b>',
		'tax_tag'    => '%1$s из "%2$s" по тегу: <b>%3$s</b>',
		// tax_tag выведет: 'тип_записи из "название_таксы" по тегу: имя_термина'.
		// Если нужны отдельные холдеры, например только имя термина, пишем так: 'записи по тегу: %3$s'
	);

	// Параметры по умолчанию
	static $args = array(
		'on_front_page'   => true,
		// выводить крошки на главной странице
		'show_post_title' => true,
		// показывать ли название записи в конце (последний элемент). Для записей, страниц, вложений
		'show_term_title' => true,
		// показывать ли название элемента таксономии в конце (последний элемент). Для меток, рубрик и других такс
		'title_patt'      => '<span class="kb_title">%s</span>',
		// шаблон для последнего заголовка. Если включено: show_post_title или show_term_title
		'last_sep'        => true,
		// показывать последний разделитель, когда заголовок в конце не отображается
		'markup'          => 'schema.org',
		// 'markup' - микроразметка. Может быть: 'rdf.data-vocabulary.org', 'schema.org', '' - без микроразметки
		// или можно указать свой массив разметки:
		// array( 'wrappatt'=>'<div class="kama_breadcrumbs">%s</div>', 'linkpatt'=>'<a href="%s">%s</a>', 'sep_after'=>'', )
		'priority_tax'    => array( 'category' ),
		// приоритетные таксономии, нужно когда запись в нескольких таксах
		'priority_terms'  => array(),
		// 'priority_terms' - приоритетные элементы таксономий, когда запись находится в нескольких элементах одной таксы одновременно.
		// Например: array( 'category'=>array(45,'term_name'), 'tax_name'=>array(1,2,'name') )
		// 'category' - такса для которой указываются приор. элементы: 45 - ID термина и 'term_name' - ярлык.
		// порядок 45 и 'term_name' имеет значение: чем раньше тем важнее. Все указанные термины важнее неуказанных...
		'nofollow'        => false,
		// добавлять rel=nofollow к ссылкам?

		// служебные
		'sep'             => '',
		'linkpatt'        => '',
		'pg_end'          => '',
	);

	function get_crumbs( $sep, $l10n, $args ) {
		global $post, $wp_query, $wp_post_types;

		self::$args['sep'] = $sep;

		// Фильтрует дефолты и сливает
		$loc = (object) array_merge( apply_filters( 'kama_breadcrumbs_default_loc', self::$l10n ), $l10n );
		$arg = (object) array_merge( apply_filters( 'kama_breadcrumbs_default_args', self::$args ), $args );

		$arg->sep = '<span class="kb_sep">' . $arg->sep . '</span>'; // дополним

		// упростим
		$sep       = &$arg->sep;
		$this->arg = &$arg;

		// микроразметка ---
		if ( 1 ) {
			$mark = &$arg->markup;

			// Разметка по умолчанию
			if ( ! $mark ) {
				$mark = array(
					'wrappatt'  => '<div class="kama_breadcrumbs">%s</div>',
					'linkpatt'  => '<a href="%s">%s</a>',
					'sep_after' => '',
				);
			} // rdf
            elseif ( $mark === 'rdf.data-vocabulary.org' ) {
				$mark = array(
					'wrappatt'  => '<div class="kama_breadcrumbs" prefix="v: http://rdf.data-vocabulary.org/#">%s</div>',
					'linkpatt'  => '<span typeof="v:Breadcrumb"><a href="%s" rel="v:url" property="v:title">%s</a>',
					'sep_after' => '</span>', // закрываем span после разделителя!
				);
			} // schema.org
            elseif ( $mark === 'schema.org' ) {
				$mark = array(
					'wrappatt'  => '<div class="kama_breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">%s</div>',
					'linkpatt'  => '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="%s" itemprop="item"><span itemprop="name">%s</span></a></span>',
					'sep_after' => '',
				);
			} elseif ( ! is_array( $mark ) ) {
				die( __CLASS__ . ': "markup" parameter must be array...' );
			}

			$wrappatt      = $mark['wrappatt'];
			$arg->linkpatt = $arg->nofollow ? str_replace( '<a ', '<a rel="nofollow"',
				$mark['linkpatt'] ) : $mark['linkpatt'];
			$arg->sep      .= $mark['sep_after'] . "\n";
		}

		$linkpatt = $arg->linkpatt; // упростим

		$q_obj = get_queried_object();

		// может это архив пустой таксы?
		$ptype = null;
		if ( empty( $post ) ) {
			if ( isset( $q_obj->taxonomy ) ) {
				$ptype = &$wp_post_types[ get_taxonomy( $q_obj->taxonomy )->object_type[0] ];
			}
		} else {
			$ptype = &$wp_post_types[ $post->post_type ];
		}

		// paged
		$arg->pg_end = '';
		if ( ( $paged_num = get_query_var( 'paged' ) ) || ( $paged_num = get_query_var( 'page' ) ) ) {
			$arg->pg_end = $sep . sprintf( $loc->paged, (int) $paged_num );
		}

		$pg_end = $arg->pg_end; // упростим

		// ну, с богом...
		$out = '';

		if ( is_front_page() ) {
			return $arg->on_front_page ? sprintf( $wrappatt,
				( $paged_num ? sprintf( $linkpatt, get_home_url(), $loc->home ) . $pg_end : $loc->home ) ) : '';
		} // страница записей, когда для главной установлена отдельная страница.
        elseif ( is_home() ) {
			$out = $paged_num ? ( sprintf( $linkpatt, get_permalink( $q_obj ),
					esc_html( $q_obj->post_title ) ) . $pg_end ) : esc_html( $q_obj->post_title );
		} elseif ( is_404() ) {
			$out = $loc->_404;
		} elseif ( is_search() ) {
			$out = sprintf( $loc->search, esc_html( $GLOBALS['s'] ) );
		} elseif ( is_author() ) {
			$tit = sprintf( $loc->author, esc_html( $q_obj->display_name ) );
			$out = ( $paged_num ? sprintf( $linkpatt,
				get_author_posts_url( $q_obj->ID, $q_obj->user_nicename ) . $pg_end, $tit ) : $tit );
		} elseif ( is_year() || is_month() || is_day() ) {
			$y_url = get_year_link( $year = get_the_time( 'Y' ) );

			if ( is_year() ) {
				$tit = sprintf( $loc->year, $year );
				$out = ( $paged_num ? sprintf( $linkpatt, $y_url, $tit ) . $pg_end : $tit );
			} // month day
			else {
				$y_link = sprintf( $linkpatt, $y_url, $year );
				$m_url  = get_month_link( $year, get_the_time( 'm' ) );

				if ( is_month() ) {
					$tit = sprintf( $loc->month, get_the_time( 'F' ) );
					$out = $y_link . $sep . ( $paged_num ? sprintf( $linkpatt, $m_url, $tit ) . $pg_end : $tit );
				} elseif ( is_day() ) {
					$m_link = sprintf( $linkpatt, $m_url, get_the_time( 'F' ) );
					$out    = $y_link . $sep . $m_link . $sep . get_the_time( 'l' );
				}
			}
		} // Древовидные записи
        elseif ( is_singular() && $ptype->hierarchical ) {
			$out = $this->_add_title( $this->_page_crumbs( $post ), $post );
		} // Таксы, плоские записи и вложения
		else {
			$term = $q_obj; // таксономии

			// определяем термин для записей (включая вложения attachments)
			if ( is_singular() ) {
				// изменим $post, чтобы определить термин родителя вложения
				if ( is_attachment() && $post->post_parent ) {
					$save_post = $post; // сохраним
					$post      = get_post( $post->post_parent );
				}

				// учитывает если вложения прикрепляются к таксам древовидным - все бывает :)
				$taxonomies = get_object_taxonomies( $post->post_type );
				// оставим только древовидные и публичные, мало ли...
				$taxonomies = array_intersect( $taxonomies,
					get_taxonomies( array( 'hierarchical' => true, 'public' => true ) ) );

				if ( $taxonomies ) {
					// сортируем по приоритету
					if ( ! empty( $arg->priority_tax ) ) {
						usort( $taxonomies, function ( $a, $b ) use ( $arg ) {
							$a_index = array_search( $a, $arg->priority_tax );
							if ( $a_index === false ) {
								$a_index = 9999999;
							}

							$b_index = array_search( $b, $arg->priority_tax );
							if ( $b_index === false ) {
								$b_index = 9999999;
							}

							return ( $b_index === $a_index ) ? 0 : ( $b_index < $a_index ? 1 : - 1 ); // меньше индекс - выше
						} );
					}

					// пробуем получить термины, в порядке приоритета такс
					foreach ( $taxonomies as $taxname ) {
						if ( $terms = get_the_terms( $post->ID, $taxname ) ) {
							// проверим приоритетные термины для таксы
							$prior_terms = &$arg->priority_terms[ $taxname ];
							if ( $prior_terms && count( $terms ) > 2 ) {
								foreach ( (array) $prior_terms as $term_id ) {
									$filter_field = is_numeric( $term_id ) ? 'term_id' : 'slug';
									$_terms       = wp_list_filter( $terms, array( $filter_field => $term_id ) );

									if ( $_terms ) {
										$term = array_shift( $_terms );
										break;
									}
								}
							} else {
								$term = array_shift( $terms );
							}

							break;
						}
					}
				}

				if ( isset( $save_post ) ) {
					$post = $save_post;
				} // вернем обратно (для вложений)
			}

			// вывод

			// все виды записей с терминами или термины
			if ( $term && isset( $term->term_id ) ) {
				$term = apply_filters( 'kama_breadcrumbs_term', $term );

				// attachment
				if ( is_attachment() ) {
					if ( ! $post->post_parent ) {
						$out = sprintf( $loc->attachment, esc_html( $post->post_title ) );
					} else {
						if ( ! $out = apply_filters( 'attachment_tax_crumbs', '', $term, $this ) ) {
							$_crumbs    = $this->_tax_crumbs( $term, 'self' );
							$parent_tit = sprintf( $linkpatt, get_permalink( $post->post_parent ),
								get_the_title( $post->post_parent ) );
							$_out       = implode( $sep, array( $_crumbs, $parent_tit ) );
							$out        = $this->_add_title( $_out, $post );
						}
					}
				} // single
                elseif ( is_single() ) {
					if ( ! $out = apply_filters( 'post_tax_crumbs', '', $term, $this ) ) {
						$_crumbs = $this->_tax_crumbs( $term, 'self' );
						$out     = $this->_add_title( $_crumbs, $post );
					}
				} // не древовидная такса (метки)
                elseif ( ! is_taxonomy_hierarchical( $term->taxonomy ) ) {
					// метка
					if ( is_tag() ) {
						$out = $this->_add_title( '', $term, sprintf( $loc->tag, esc_html( $term->name ) ) );
					} // такса
                    elseif ( is_tax() ) {
						$post_label = $ptype->labels->name;
						$tax_label  = $GLOBALS['wp_taxonomies'][ $term->taxonomy ]->labels->name;
						$out        = $this->_add_title( '', $term,
							sprintf( $loc->tax_tag, $post_label, $tax_label, esc_html( $term->name ) ) );
					}
				} // древовидная такса (рибрики)
				else {
					if ( ! $out = apply_filters( 'term_tax_crumbs', '', $term, $this ) ) {
						$_crumbs = $this->_tax_crumbs( $term, 'parent' );
						$out     = $this->_add_title( $_crumbs, $term, esc_html( $term->name ) );
					}
				}
			} // влоежния от записи без терминов
            elseif ( is_attachment() ) {
				$parent      = get_post( $post->post_parent );
				$parent_link = sprintf( $linkpatt, get_permalink( $parent ), esc_html( $parent->post_title ) );
				$_out        = $parent_link;

				// вложение от записи древовидного типа записи
				if ( is_post_type_hierarchical( $parent->post_type ) ) {
					$parent_crumbs = $this->_page_crumbs( $parent );
					$_out          = implode( $sep, array( $parent_crumbs, $parent_link ) );
				}

				$out = $this->_add_title( $_out, $post );
			} // записи без терминов
            elseif ( is_singular() ) {
				$out = $this->_add_title( '', $post );
			}
		}

		// замена ссылки на архивную страницу для типа записи
		$home_after = apply_filters( 'kama_breadcrumbs_home_after', '', $linkpatt, $sep, $ptype );

		if ( '' === $home_after ) {
			// Ссылка на архивную страницу типа записи для: отдельных страниц этого типа; архивов этого типа; таксономий связанных с этим типом.
			if ( $ptype && $ptype->has_archive && ! in_array( $ptype->name, array( 'post', 'page', 'attachment' ) )
			     && ( is_post_type_archive() || is_singular() || ( is_tax() && in_array( $term->taxonomy,
							$ptype->taxonomies ) ) )
			) {
				$pt_title = $ptype->labels->name;

				// первая страница архива типа записи
				if ( is_post_type_archive() && ! $paged_num ) {
					$home_after = $pt_title;
				} // singular, paged post_type_archive, tax
				else {
					$home_after = sprintf( $linkpatt, get_post_type_archive_link( $ptype->name ), $pt_title );

					$home_after .= ( ( $paged_num && ! is_tax() ) ? $pg_end : $sep ); // пагинация
				}
			}
		}

		$before_out = sprintf( $linkpatt, home_url(),
				$loc->home ) . ( $home_after ? $sep . $home_after : ( $out ? $sep : '' ) );

		$out = apply_filters( 'kama_breadcrumbs_pre_out', $out, $sep, $loc, $arg );

		$out = sprintf( $wrappatt, $before_out . $out );

		return apply_filters( 'kama_breadcrumbs', $out, $sep, $loc, $arg );
	}

	function _page_crumbs( $post ) {
		$parent = $post->post_parent;

		$crumbs = array();
		while ( $parent ) {
			$page     = get_post( $parent );
			$crumbs[] = sprintf( $this->arg->linkpatt, get_permalink( $page ), esc_html( $page->post_title ) );
			$parent   = $page->post_parent;
		}

		return implode( $this->arg->sep, array_reverse( $crumbs ) );
	}

	function _tax_crumbs( $term, $start_from = 'self' ) {
		$termlinks = array();
		$term_id   = ( $start_from === 'parent' ) ? $term->parent : $term->term_id;
		while ( $term_id ) {
			$term        = get_term( $term_id, $term->taxonomy );
			$termlinks[] = sprintf( $this->arg->linkpatt, get_term_link( $term ), esc_html( $term->name ) );
			$term_id     = $term->parent;
		}

		if ( $termlinks ) {
			return implode( $this->arg->sep, array_reverse( $termlinks ) ) /*. $this->arg->sep*/
				;
		}

		return '';
	}

	// добалвяет заголовок к переданному тексту, с учетом всех опций. Добавляет разделитель в начало, если надо.
	function _add_title( $add_to, $obj, $term_title = '' ) {
		$arg        = &$this->arg; // упростим...
		$title      = $term_title ? $term_title : esc_html( $obj->post_title ); // $term_title чиститься отдельно, теги моугт быть...
		$show_title = $term_title ? $arg->show_term_title : $arg->show_post_title;

		// пагинация
		if ( $arg->pg_end ) {
			$link   = $term_title ? get_term_link( $obj ) : get_permalink( $obj );
			$add_to .= ( $add_to ? $arg->sep : '' ) . sprintf( $arg->linkpatt, $link, $title ) . $arg->pg_end;
		} // дополняем - ставим sep
        elseif ( $add_to ) {
			if ( $show_title ) {
				$add_to .= $arg->sep . sprintf( $arg->title_patt, $title );
			} elseif ( $arg->last_sep ) {
				$add_to .= $arg->sep;
			}
		} // sep будет потом...
        elseif ( $show_title ) {
			$add_to = sprintf( $arg->title_patt, $title );
		}

		return $add_to;
	}

}


/**
 * обязательный для темы плагин kama thumbnail
 */
//if ( ! is_admin() && ! function_exists( 'kama_thumb_img' ) ) {
//	wp_die( 'Активируйте обязательный для темы плагин Kama Thumbnail' );
//}


/**
 * @param $post_id
 * Считает популярность записей
 */
function shapeSpace_popular_posts( $post_id ) {
	if ( is_user_logged_in() && ! 'sas_realty' == get_post_type() ) {
		return;
	}
	$count_key = 'popular_posts';
	$count     = get_post_meta( $post_id, $count_key, true );
	if ( $count == '' ) {
		$count = 0;
		delete_post_meta( $post_id, $count_key );
		add_post_meta( $post_id, $count_key, '0' );
	} else {
		$count ++;
		update_post_meta( $post_id, $count_key, $count );
	}
}

function shapeSpace_track_posts( $post_id ) {
	if ( ! is_single() ) {
		return;
	}
	if ( empty( $post_id ) ) {
		global $post;
		$post_id = $post->ID;
	}
	shapeSpace_popular_posts( $post_id );
}

add_action( 'wp_head', 'shapeSpace_track_posts' );


/**
 * Добавление виджета Популярные объявления
 */
class Popular_Property extends WP_Widget {
	public function __construct() {
		parent::__construct(
			'popular_property_widget', // Base ID
			'Популярные объявления'// Name
		);
	}

	public function widget( $args, $instance ) {
		$popular = new WP_Query( array(
			'post_type'      => 'sas_realty',
			'post_status'      => 'publish',
			'posts_per_page' => 4,
			'meta_key'       => 'popular_posts',
			'orderby'        => 'meta_value_num',
			'order'          => 'DESC'
		) );
		?>
        <div class="sidebar__realty">
            <h4>Популярные объявления</h4>
			<?php
			while ( $popular->have_posts() ) : $popular->the_post(); ?>
				<?php $thumbnail_from_gallery = get_field( 'sas_realty_gallery' ) ?>
                <a href="<?php the_permalink(); ?>" class="sidebar__realty__item">
                    <img src="<?= kama_thumb_src( 'w=212 &h=120', $thumbnail_from_gallery[0]['url'] ) ?>"
                         alt="<?= $thumbnail_from_gallery[0]['alt'] ?>"
                         title="Кликните чтобы перейти на страницу <?= get_the_title() ?>" class="img-responsive">
                    <h5><?php the_title(); ?></h5>
                </a>
			<?php endwhile;
			wp_reset_postdata(); ?>
        </div>
		<?php
	}
}

add_action( 'widgets_init', function () {
	register_widget( 'Popular_Property' );
} );


/**
 * Добавление виджета Популярные объявления
 */
class Zhilyye_Kompleksy extends WP_Widget {
	public function __construct() {
		parent::__construct(
			'zhilyye_kompleksy_widget', // Base ID
			'Жилые комплексы'// Name
		);
	}

	public function widget( $args, $instance ) {
		global $post; // не обязательно
		$args            = array( 'category_name' => 'zhilyye-kompleksy' ); // 5 записей из рубрики 9
		$sidebar_content = get_posts( $args );
		?>
        <div class="sidebar__realty">
            <h4>Жилые комплексы в Иркутске</h4>
			<?php
			foreach ( $sidebar_content as $post ) {
				setup_postdata( $post );
				?>
                <a href="<?php the_permalink() ?>" class="sidebar__realty__item">
					<?php $title = get_the_title() ?>
					<?= kama_thumb_img( "w=170 &h=100 &class=img-responsive &alt=$title &title=Кликните чтобы перейти на страницу $title" ); ?>
                    <h5><?php the_title(); ?></h5>
                </a>
				<?php
			}
			?>
        </div>
		<?php
		wp_reset_postdata(); // сбрасываем переменную $post
	}
}

add_action( 'widgets_init', function () {
	register_widget( 'Zhilyye_Kompleksy' );
} );


/**
 * Добавление виджета Ваш агент по недвижимости
 */
class Estate_Agents extends WP_Widget {
	public function __construct() {
		parent::__construct(
			'estate_agents_widget', // Base ID
			'Агенты недвижимости'// Name
		);
	}

	public function widget( $args, $instance ) {
		?>
        <div class="sidebar__agents">
			<?php
			$uri = get_template_directory_uri();
			$img_link = "$uri/assets/img/LZ.jpg"
			?>
            <img src="<?= kama_thumb_src( 'w=110 &h=110', $img_link ) ?>" alt="Доенина Любовь Залинуровна"
                 class="img-responsive">
            <h5>Доенина Любовь Залинуровна</h5>
            <p class="description">
                <small>Ваш персональный риелтор со стажем <strong>более 9 лет.</strong></small>
            </p>
            <p><strong>8 964 218-18-99</strong></p>
            <a href="#" class="sas-button">Задать вопрос</a>
        </div>
		<?php
	}
}

add_action( 'widgets_init', function () {
	register_widget( 'Estate_Agents' );
} );


/**
 * Регистрирую таксономию Жилые комплексы
 */
function create_zhilyye_kompleksy_tax() {
	register_taxonomy( 'zhilyye-kompleksy', 'sas_realty', array(
		'label'   => __( 'ЖK', 'sas' ),
		'rewrite' => array(
			'slug' => 'zhilyye-kompleksy',
            'with_front'=>false
        ),
		'hierarchical' => true,
	) );
}
add_action( 'init', 'create_zhilyye_kompleksy_tax', 0 );

/**
 * Опиши функцию
 */
add_action( 'admin_post_sas_test_function', 'sas_test_function' );
add_action( 'admin_post_nopriv_sas_test_function', 'sas_test_function' );
function sas_test_function() {
	echo "<h1>" . $_GET['user'] . "</h1>";
}