<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sas
 */

?>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 logo-wrapper">
				<?= do_shortcode( '[logo]' ); ?>
            </div>
            <div class="col-md-6 col-sm-4">
                <ul class="social-icons social-icons--rounded">
                    <li class="social-icons__item">
                        <a href="#" class="social-icons__item__link social-icons__item__link--vk">
                            <svg xmlns="http://www.w3.org/2000/svg" width="548" height="548"
                                 viewBox="0 0 548.4 548.4">
                                <path d="M545.5 400.3c-0.7-1.4-1.3-2.6-1.9-3.6 -9.5-17.1-27.7-38.2-54.5-63.1l-0.6-0.6 -0.3-0.3 -0.3-0.3h-0.3c-12.2-11.6-19.9-19.4-23.1-23.4 -5.9-7.6-7.2-15.3-4-23.1 2.3-5.9 10.9-18.4 25.7-37.4 7.8-10.1 14-18.2 18.6-24.3 32.9-43.8 47.2-71.8 42.8-83.9l-1.7-2.8c-1.1-1.7-4.1-3.3-8.8-4.7 -4.8-1.4-10.9-1.7-18.3-0.7l-82.2 0.6c-1.3-0.5-3.2-0.4-5.7 0.1 -2.5 0.6-3.7 0.9-3.7 0.9l-1.4 0.7 -1.1 0.9c-1 0.6-2 1.6-3.1 3 -1.1 1.4-2.1 3.1-2.8 5 -9 23-19.1 44.4-30.6 64.2 -7 11.8-13.5 22-19.4 30.7 -5.9 8.7-10.8 15-14.8 19.1 -4 4.1-7.6 7.4-10.9 9.8 -3.2 2.5-5.7 3.5-7.4 3.1 -1.7-0.4-3.3-0.8-4.9-1.1 -2.7-1.7-4.8-4-6.4-7 -1.6-2.9-2.7-6.7-3.3-11.1 -0.6-4.5-0.9-8.3-1-11.6 -0.1-3.2 0-7.8 0.1-13.7 0.2-5.9 0.3-9.9 0.3-12 0-7.2 0.1-15.1 0.4-23.6 0.3-8.5 0.5-15.2 0.7-20.1 0.2-4.9 0.3-10.2 0.3-15.7s-0.3-9.8-1-13c-0.7-3.1-1.7-6.2-3-9.1 -1.3-2.9-3.3-5.2-5.9-6.9 -2.6-1.6-5.8-2.9-9.6-3.9 -10.1-2.3-22.9-3.5-38.5-3.7 -35.4-0.4-58.1 1.9-68.2 6.9 -4 2.1-7.6 4.9-10.8 8.6 -3.4 4.2-3.9 6.5-1.4 6.9 11.4 1.7 19.5 5.8 24.3 12.3l1.7 3.4c1.3 2.5 2.7 6.9 4 13.1 1.3 6.3 2.2 13.2 2.6 20.8 1 13.9 1 25.8 0 35.7 -1 9.9-1.9 17.6-2.7 23.1 -0.9 5.5-2.1 10-3.9 13.4 -1.7 3.4-2.9 5.5-3.4 6.3 -0.6 0.8-1 1.2-1.4 1.4 -2.5 0.9-5 1.4-7.7 1.4 -2.7 0-5.9-1.3-9.7-4 -3.8-2.7-7.8-6.3-11.8-11 -4.1-4.7-8.7-11.2-13.8-19.6 -5.1-8.4-10.5-18.3-16-29.7l-4.6-8.3c-2.9-5.3-6.8-13.1-11.7-23.3 -5-10.2-9.3-20-13.1-29.6 -1.5-4-3.8-7-6.9-9.1l-1.4-0.9c-0.9-0.8-2.5-1.6-4.6-2.4 -2.1-0.9-4.3-1.5-6.6-1.9l-78.2 0.6c-8 0-13.4 1.8-16.3 5.4l-1.1 1.7C0.3 140.1 0 141.7 0 143.8c0 2.1 0.6 4.7 1.7 7.7 11.4 26.8 23.8 52.7 37.3 77.7 13.4 24.9 25.1 45 35 60.2 9.9 15.2 20 29.6 30.3 43.1 10.3 13.5 17.1 22.2 20.4 26 3.3 3.8 6 6.7 7.9 8.6l7.1 6.9c4.6 4.6 11.3 10 20.1 16.4 8.9 6.4 18.7 12.7 29.4 18.9 10.8 6.2 23.3 11.2 37.5 15.1 14.3 3.9 28.2 5.5 41.7 4.7h32.8c6.7-0.6 11.7-2.7 15.1-6.3l1.1-1.4c0.8-1.1 1.5-2.9 2.1-5.3 0.7-2.4 1-5 1-7.9 -0.2-8.2 0.4-15.6 1.9-22.1 1.4-6.6 3-11.5 4.9-14.8 1.8-3.3 3.9-6.1 6.1-8.4 2.3-2.3 3.9-3.7 4.9-4.1 0.9-0.5 1.7-0.8 2.3-1 4.6-1.5 9.9 0 16.1 4.4 6.2 4.5 12 10 17.4 16.6 5.4 6.6 11.9 13.9 19.6 22.1 7.6 8.2 14.3 14.3 20 18.3l5.7 3.4c3.8 2.3 8.8 4.4 14.9 6.3 6.1 1.9 11.4 2.4 16 1.4l73.1-1.1c7.2 0 12.9-1.2 16.8-3.6 4-2.4 6.4-5 7.1-7.9 0.8-2.9 0.8-6.1 0.1-9.7C546.8 404.3 546.1 401.7 545.5 400.3z"/>
                            </svg>
                        </a>
                    </li>
                    <li class="social-icons__item">
                        <a href="#" class="social-icons__item__link social-icons__item__link--twitter">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612">
                                <path d="M612 116.3c-22.5 10-46.7 16.8-72.1 19.8 25.9-15.5 45.8-40.2 55.2-69.4 -24.3 14.4-51.2 24.8-79.8 30.5 -22.9-24.4-55.5-39.7-91.6-39.7 -69.3 0-125.6 56.2-125.6 125.5 0 9.8 1.1 19.4 3.3 28.6C197.1 206.3 104.6 156.3 42.6 80.4c-10.8 18.5-17 40.1-17 63.1 0 43.6 22.2 82 55.8 104.5 -20.6-0.7-39.9-6.3-56.9-15.8v1.6c0 60.8 43.3 111.6 100.7 123.1 -10.5 2.8-21.6 4.4-33.1 4.4 -8.1 0-15.9-0.8-23.6-2.3 16 49.9 62.3 86.2 117.3 87.2 -42.9 33.7-97.1 53.7-155.9 53.7 -10.1 0-20.1-0.6-29.9-1.7 55.6 35.7 121.5 56.5 192.4 56.5 230.9 0 357.2-191.3 357.2-357.2l-0.4-16.3C573.9 163.5 595.2 141.4 612 116.3z"/>
                            </svg>
                        </a>
                    </li>
                    <li class="social-icons__item">
                        <a href="#" class="social-icons__item__link social-icons__item__link--youtube">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 310 310">
                                <path d="M297.9 64.6c-11.2-13.3-31.8-18.7-71.3-18.7H83.4c-40.4 0-61.4 5.8-72.5 19.9C0 79.7 0 100 0 128.2v53.7c0 54.6 12.9 82.2 83.4 82.2h143.2c34.2 0 53.2-4.8 65.4-16.5C304.6 235.5 310 215.9 310 181.8v-53.7C310 98.5 309.2 78 297.9 64.6zM199 162.4l-65 34c-1.5 0.8-3 1.1-4.6 1.1 -1.8 0-3.6-0.5-5.2-1.4 -3-1.8-4.8-5.1-4.8-8.6v-67.8c0-3.5 1.8-6.7 4.8-8.5 3-1.8 6.7-1.9 9.8-0.3l65 33.8c3.3 1.7 5.4 5.1 5.4 8.9C204.4 157.3 202.3 160.7 199 162.4z"/>
                            </svg>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-4">
                <div class="contact-item footer__contact-item contact-item__phone footer__contact-item__phone">
                    <a href="tel:89500774502" class="contacts__item phone reset" title="Позвонить">
                        <object class="svg-icon" type="image/svg+xml"
                                data="<?= get_template_directory_uri() ?>/assets/img/icon/smartphone.svg"></object>
                        <span>8 924 705 72 67</span>
                    </a>
                </div>
                <div class="contact-item footer__contact-item contact-item__email footer__contact-item__email">
                    <a href="mailto:info@im38.ru?subject=Вопрос из сайта." class="contacts__item email reset"
                       title="Написать письмо">
                        <object class="svg-icon" type="image/svg+xml"
                                data="<?= get_template_directory_uri() ?>/assets/img/icon/email.svg"></object>
                        <span>info@im38.ru</span>
                    </a>
                </div>
            </div>
            <div class="col-sm-12">
                <small class="copyright">Агентсво недвижимости «Имущество». 2008 — <?= date( 'Y' ) ?>. Все права
                    защищены. Копирование запрещено.
                </small>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
<!--<script async defer src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyD0_saWLBc49Q9VFQ7mkg12VrN6EX2Pjkc&callback=initMap"></script>-->
<script src="<?= get_template_directory_uri() ?>/assets/js/scripts.min.js"></script>

</body>
</html>
