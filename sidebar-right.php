<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sas
 */
if ( ! is_active_sidebar( 'sidebar-right' ) ) {
	return;
}
global $post; // не обязательно
$args            = array( 'category_name' => 'zhilyye-kompleksy' ); // 5 записей из рубрики 9
$sidebar_content = get_posts( $args );
?>
<aside class="sidebar__realty">
    <h4>Жилые комплексы в Иркутске</h4>
	<?php
	foreach ( $sidebar_content as $post ) {
		setup_postdata( $post );
		?>
        <a href="<?php the_permalink() ?>" class="sidebar__realty__item">
			<?php $title = get_the_title() ?>
			<?= kama_thumb_img( "w=170 &h=100 &class=img-responsive &alt=$title &title=Кликните чтобы перейти на страницу $title" ); ?>
            <h5><?php the_title(); ?></h5>
        </a>
		<?php
	}
	?>
</aside>
<?php
wp_reset_postdata(); // сбрасываем переменную $post
?>
