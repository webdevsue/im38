<?php
/**
 * Created by PhpStorm.
 * User: alexa
 * Date: 17.01.2017
 * Time: 14:12
 */
?>
<?php get_header(); ?>

    <!-- slider -->
    <ul class="main-screen-slider">
        <li class="slide" style="background-image: url(<?= get_template_directory_uri() ?>/assets/img/bg3.jpg);">
            <div class="slide-content container c-white">
                <h2>Более 9 лет успешной работы с военной ипотекой</h2>
                <p>За все эти годы мы отточили свое мастерство так, что с полной уверенностью можем сказать: «Никто в
                    Иркутске не сделает это лучше нас!»</p>
                <a href="#" class="sas-button main-btn">узнать подробнее</a>
            </div>
        </li>
        <li class="slide" style="background-image: url(<?= get_template_directory_uri() ?>/assets/img/bg1.jpg);">
            <div class="slide-content container">
                <h2>Основной заголовок слайдера</h2>
                <p>Расположите элементы в желаемом порядке путём перетаскивания. Можно также щёлкнуть на стрелку
                    справа от элемента, чтобы открыть дополнительные настройки.</p>
                <a href="#" class="sas-button main-btn">узнать подробнее</a>
            </div>
        </li>
    </ul>
    <!-- slider / End -->
    <!-- why we -->
    <section class="section s-whywe">
        <h2 class="s-title">Почему вы должны выбрать именно нас</h2>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="s-whywe__wrapper">
                        <img class="s-whywe__icon" src="<?= get_template_directory_uri() ?>/assets/img/search.svg"
                             alt="">
                        <div class="s-whywe__text">
                            <div class="s-whywe__text__title">Первый заголовок</div>
                            <div class="s-whywe__text__description">Мы, узкоспециализированная компания, которая
                                оказывает полный
                                сервисный пакет сделки с недвижимостью для военнослужащих, быстро и эффективно.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="s-whywe__wrapper">
                        <img class="s-whywe__icon" src="<?= get_template_directory_uri() ?>/assets/img/property.svg"
                             alt="">
                        <div class="s-whywe__text">
                            <div class="s-whywe__text__title">Второй заголовок</div>
                            <div class="s-whywe__text__description">Мы, узкоспециализированная компания, которая
                                оказывает
                                полный
                                сервисный пакет сделки с недвижимостью для военнослужащих, быстро и эффективно, быстро и
                                эффективно.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="s-whywe__wrapper">
                        <img class="s-whywe__icon" src="<?= get_template_directory_uri() ?>/assets/img/home.svg" alt="">
                        <div class="s-whywe__text">
                            <div class="s-whywe__text__title">Третий заголовок</div>
                            <div class="s-whywe__text__description">Полный сервисный пакет сделки с недвижимостью для
                                военнослужащих,
                                быстро и эффективно, быстро и эффективно.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- why we / End -->
    <!-- filter -->
    <section class="section s-searchform">
        <h2 class="s-title">Подберите интересующую вас недвижимость</h2>
        <div class="container__bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <?= do_shortcode('[main_search_form class="asdasd"]') ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- filter / End -->
<?php get_footer(); ?>